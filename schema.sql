CREATE TABLE plays (
  id INTEGER PRIMARY KEY,
  artist VARCHAR NOT NULL,
  title VARCHAR NOT NULL,
  additional_info VARCHAR,
  ts INT NOT NULL
);
